<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "activity".
 *
 * @property integer $Id
 * @property string $title
 * @property integer $categoryid
 * @property integer $status
 */
class Activity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Id', 'title', 'categoryid', 'status'], 'required'],
            [['Id', 'categoryid', 'status'], 'integer'],
            [['title'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'title' => 'Title',
            'categoryid' => 'Categoryid',
            'status' => 'Status',
        ];
    }
}
