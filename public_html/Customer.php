<?php

class Customer
{
	protected $_name;
	protected $_Email;
	protected $_phone;
	
	public static $_count = 0; 
     
    public function __construct($name, $Email, $phone) 
    { 
        if(strlen($name)<1 || (strlen(preg_replace('/\s+/', '', $name)))<1) throw new Exception("Name Is not valid for Customer"); else $this->_name = $name; 
        if(strlen($phone)<7) throw new Exception("Phone Is not valid for Customer"); else $this->_phone = $phone; 
        if(strlen($Email)<1 || (strlen(preg_replace('/\s+/', '', $Email)))<1)throw new Exception("Email Is not valid for Customer"); else $this->_Email = $Email; 
         
        ++self::$_count; 
    } 
	
	public function __destruct()
	{
		--self::$_count;
	}
	
	public function show() 
    { 
         return "user name: ". $this-> _name ."<br />". 
                "email: ". $this-> _Email ."<br />". 
                "phone number: ". $this-> _phone ."<br />". 
                "<br />"; 
				
    } 
	public static function counter() 
                {         
                    return "num of customers in the system:" .self::$_count ."<br><br><br>" ; 
                }   	
}

class VipCustomer extends Customer
{
	private $_loyaltyPoints;
	public function __construct($name, $Email, $phone, $loyaltyPoints)
	{
		parent::__construct($name, $Email, $phone);
		$this->_loyaltyPoints = $loyaltyPoints;  
	}
	public function show() 
    { 
	echo parent::show();
	            echo   'Customer Loyalty Points: ' . VipCustomer::getPoints() .'<br><br><br><br>' ; 	
	}
	 private function getPoints() 
        { 
                return $this->_loyaltyPoints;             
        } 
}


$obj1 = new Customer('jason B','aaaa@gamil.com','0558726636');  
$obj2 = new Customer('nick N','nick@gamil.com','0505544838');  
$obj3 = new VipCustomer('elton john','elton@john.com','0522333444', '4470');  
echo $obj1->show();
echo $obj2->show();
echo $obj3->show();
echo Customer::counter();
unset($obj1); 
echo $obj2->show();         
echo $obj3->show();        
echo Customer::counter();
?>