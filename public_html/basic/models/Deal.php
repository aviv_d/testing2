<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "deal".
 *
 * @property integer $Id
 * @property integer $leadId
 * @property string $name
 * @property integer $amount
 */
class Deal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Id', 'leadId', 'name', 'amount'], 'required'],
            [['Id', 'leadId', 'amount'], 'integer'],
            [['name'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'leadId' => 'Lead ID',
            'name' => 'Name',
            'amount' => 'Amount',
        ];
    }
	
	/*public static function getLeadsWithAllLeads()
	{
		$users = self::getUsers();
		$users[-1] = 'All Users';
		$users = array_reverse ( $users, true );
		return $users;	
	}*/
public static function getLeads()
{
$allLeads = self::find()->all();
$allLeadsArray = ArrayHelper::
													map($allLeads, 'leadId',  'name' );
													return $allLeadsArray;
}	
}
